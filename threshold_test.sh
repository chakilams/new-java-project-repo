cat behaviors.csv | sed 's/"//g' > behaviors.txt
line=$(tail -n 1 behaviors.txt)
IFS=','
read -a strarr <<< "$line"
Failed=${strarr[3]}
Passed=${strarr[5]}
Total=$(($Failed + $Passed))
calc=$(echo $Passed $Total | awk '{ printf "%f", $1 / $2 }')
perc=$(echo $calc 100 | awk '{ printf "%i", $1 * $2 }')
thrsh=$(echo 0.7 100 | awk '{ printf "%i", $1 * $2 }')
if [ $perc -ge $thrsh ]
    then
	{
        echo "test results are passed"
        exit 0
	}
    else
	{
    	echo "test results are not passed due to threshold"
        exit 1
	}
fi